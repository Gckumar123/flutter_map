import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'locations.dart' as locations;

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final Map<String, Marker> _markers = {};
  Future<void> _onMapCreated(GoogleMapController controller) async {
    final googleOffices = await locations.getGoogleOffices();
    setState(() {
      _markers.clear();
      for (final office in googleOffices.offices) {
        final marker = Marker(
          markerId: MarkerId(office.name),
          position: LatLng(office.lat, office.lng),
          onTap: (){
            //_showModal();
            //_settingModalBottomSheet(context);
          },
          infoWindow: InfoWindow(
            title: office.name+"chaitanya",
            snippet: office.address,
          ),
        );
        _markers[office.name] = marker;
      }
    });
  }


  PersistentBottomSheetController _controller;
  GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        key: _key,
        appBar: AppBar(
          title: const Text('Google Office Locations'),
          backgroundColor: Colors.green[700],
        ),
        body:Builder(
        builder: (context) => GoogleMap(
          zoomControlsEnabled: false,
          mapType: MapType.normal,
          onMapCreated: (GoogleMapController controller) async{
            final googleOffices = await locations.getGoogleOffices();

            setState(() {
              _markers.clear();
              for (final office in googleOffices.offices) {
                final marker = Marker(
                  markerId: MarkerId(office.name),
                  position: LatLng(office.lat, office.lng),
                  onTap: (){
                    _controller = _key.currentState.showBottomSheet(
                            (_) => SizedBox(
                          child: Container(
                            height: 200,
                            color: Colors.white,
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                      Text('Longitude : ',),
                                      Text(office.lng.toString(),style: new TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.red,
                                      )),

                                    ],
                                    ),


                                  ),
                                  Container(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text('Latitude : ',),
                                        Text(office.lat.toString(),style: new TextStyle(
                                          fontSize: 20.0,
                                          color: Colors.red,
                                        )),

                                      ],
                                    ),


                                  ),
                                  Container(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text('Region : ',),
                                        Text(office.region.toString(),style: new TextStyle(
                                          fontSize: 20.0,
                                          color: Colors.red,
                                        )),

                                      ],
                                    ),


                                  ),
                                  Expanded(
                                    child: Align(
                                      alignment: FractionalOffset.bottomCenter,
                                      child: Padding(
                                          padding: EdgeInsets.only(bottom: 10.0),
                                          child: RaisedButton(
                                            child: const Text('Close BottomSheet'),
                                            onPressed: () => Navigator.pop(context),
                                          ),
                                      )
                                    ),
                                  ),

                                ],
                              ),
                            ),
                          ),

                          width: double.maxFinite,
                        ),
                    );
                    //_settingModalBottomSheet(context);
                  },
                );
                _markers[office.name] = marker;
              }
            });
          },
          initialCameraPosition: CameraPosition(
            target: const LatLng(0, 0),
            zoom: 2,
          ),
          markers: _markers.values.toSet(),
        ),
        ),
      ),
    );
  }
}